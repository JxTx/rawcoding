#+TITLE: Making a Blog - ASP.NET Core Beginner Tutorials

Decided to learn dotnet and what better way to do that than make something?

I followed [[https://www.youtube.com/watch?v=xEQYwMc-LiQ&list=PLOeFnOV9YBa6dkT4-FxFXtS9Xr-mfE09y][this]] playlist, but things started to break after episode 15 or 16, so I decided to move on, and maybe revisit when I get better.
