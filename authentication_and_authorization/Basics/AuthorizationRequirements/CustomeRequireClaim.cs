﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Basics.AuthorizationRequirements
{
    public class CustomeRequireClaim : IAuthorizationRequirement
    {
        public CustomeRequireClaim(string claimType)
        {
            ClaimType = claimType;
        }
        public string ClaimType { get; }
    }

    public class CustomeRequireClaimHandler : AuthorizationHandler<CustomeRequireClaim>
    {
        protected override Task HandleRequirementAsync(
            AuthorizationHandlerContext context, 
            CustomeRequireClaim requirement)
        {
            var hasClaim = context.User.Claims.Any(x => x.Type == requirement.ClaimType);
            if(hasClaim)
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }

    public static class AuthorizationPolicyBuilderExtensions
    {
        public static AuthorizationPolicyBuilder RequireCustomClaim(
            this AuthorizationPolicyBuilder builder,
            string claimType)
        {
            builder.AddRequirements(new CustomeRequireClaim(claimType));
            return builder
        }
    }
}
